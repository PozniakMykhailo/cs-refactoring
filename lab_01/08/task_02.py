import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (sin(x) + cos(2 + y)) / (cos(x) + sin( 2- x))
Z2 = (sin(x) - 5) / (x- (cos(2))^2)
Z3 = (15 * Z1^2) / (10 - Z2^2)
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        d = math.cos(x) + math.sin(2 - x)
        if math.cos(x) + math.sin(2 - x) != 0:
            z1 = (math.sin(x) + math.cos(2 + y)) / d
            d = x - (math.cos(2) ** 2)
            if x - (math.cos(2) ** 2) != 0:
                z2 = (math.sin(x) - 5) / d
                d = 10 - (z2 ** 2)
                if 10 - (z2 ** 2) != 0:
                    z3 = (15 * z1 ** 2) / d
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
