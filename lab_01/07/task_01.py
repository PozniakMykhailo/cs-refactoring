import math
import sys

'''
Обчислити площу трикутника, якщо задано довжини сторін трикутника.
'''


def main():
    try:
        a = float(input("Введіть довжину першої сторони: "))
        b = float(input("Введіть довжину другої сторони: "))
        c = float(input("Введіть довжину третьої сторони: "))
        if a + b > c and a + c > b and b + c > a:
            p = (a + b + c) / 2
            area = math.sqrt(p * (p - a) * (p - b) * (p - c))
            print(f"Площа трикутника: {area}")
            sys.exit(0)
        else:
            print("Трикутник з такими сторонами не існує.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для довжин сторін.")
        sys.exit(1)


if __name__ == "__main__":
    main()
