import unittest
from unittest.mock import patch
from io import StringIO
from task_02 import main

class TestFunc(unittest.TestCase):
    @patch('sys.stdin', StringIO('слово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_a(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Некоректні дані. Введіть числові значення для A, B, x та y.\n")
    
    @patch('sys.stdin', StringIO('3\nслово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_b(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Некоректні дані. Введіть числові значення для A, B, x та y.\n")

    @patch('sys.stdin', StringIO('3\n4\nслово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_x(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Некоректні дані. Введіть числові значення для A, B, x та y.\n")
    
    @patch('sys.stdin', StringIO('3\n4\n5\nслово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_y(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Некоректні дані. Введіть числові значення для A, B, x та y.\n")

    @patch('sys.stdin', StringIO('1\n0\n1\n0\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_znamennuk_zero(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")

    @patch('sys.stdin', StringIO('1\n1\n1\n1\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_well_done(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Z1 = 0.12019830702311476\nZ2 = -0.4161468365471424\nZ3 = 0.0834264005602956\n")

if __name__ == '__main__':
    unittest.main()
