import unittest
from unittest.mock import patch
from io import StringIO
from ref_task_01 import main

class TestFunc(unittest.TestCase):
    @patch('sys.stdin', StringIO('слово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_gipotenuza(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введене значення не є числом.\n")
    
    @patch('sys.stdin', StringIO('4\nкартопля\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_katet(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введіть довжину катета:Введене значення не є числом.\n")
    
    @patch('sys.stdin', StringIO('-7\n6\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_gipot_zero(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введіть довжину катета:Довжина гіпотенузи (-7.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('7\n-6\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_katet_zero(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введіть довжину катета:Довжина катета (-6.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('4\n5\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_znachenya(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введіть довжину катета:Довжина гіпотенузи (4.0) має бути більшою за довжину катета 5.0.\n")

    @patch('sys.stdin', StringIO('5\n4\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_well_done(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введіть довжину катета:Довжина другого катета дорівнює 3.0.\n")

if __name__ == '__main__':
    unittest.main()
