import sys
from math import sqrt

class Application:
    v1 = None
    v2 = None

    def inputX(self):
        try:
            self.v1 = float(input('Введіть довжину гіпотенузи:'))
            self.v2 = float(input('Введіть довжину катета:'))
        except ValueError:
            print('Введене значення не є числом.')
            sys.exit(1)

    def validation(self):
        if self.v1 <= 0:
            print(f"Довжина гіпотенузи ({self.v1}) не може бути менше або дорівнювати 0.")
            sys.exit(1)
        elif self.v2 <= 0:
            print(f"Довжина катета ({self.v2}) не може бути менше або дорівнювати 0.")
            sys.exit(1)
        elif self.v1 < self.v2:
            print(f"Довжина гіпотенузи ({self.v1}) має бути більшою за довжину катета {self.v2}.")
            sys.exit(1)

    def printf(self):
        print(f"Довжина другого катета дорівнює {sqrt(self.v1 ** 2 - self.v2 ** 2)}.")
        sys.exit(0)

def main():
    app = Application()
    app.inputX()
    app.validation()
    app.printf()

if __name__ == '__main__':
    main()
