import unittest
from unittest.mock import patch
from io import StringIO
from ref_task_01 import main

class TestFunc(unittest.TestCase):
    @patch('sys.stdin', StringIO('слово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_a(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Некоректні дані. Введіть числові значення для A, B, x та y.\n")
    
    @patch('sys.stdin', StringIO('1\nслово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_b(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Некоректні дані. Введіть числові значення для A, B, x та y.\n")

    @patch('sys.stdin', StringIO('1\n1\nслово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_x(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Некоректні дані. Введіть числові значення для A, B, x та y.\n")

    @patch('sys.stdin', StringIO('1\n1\n1\nлово\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_y(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Некоректні дані. Введіть числові значення для A, B, x та y.\n")

    @patch('sys.stdin', StringIO('1\n0\n1\n0\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_znamennuk_z1(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")

    @patch('sys.stdin', StringIO('2\n1\n0\n0\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_znamennuk_z2(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")

    @patch('sys.stdin', StringIO('1\n1\n1\n0\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_znamennuk_z3(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")

    @patch('sys.stdin', StringIO('5\n4\n3\n2\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_znamennuk_z3(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Z1 = 0.7344517332210917\nZ2 = -0.2958129155327455\nZ3 = 4.19240401238303\n")


if __name__ == '__main__':
    unittest.main()
