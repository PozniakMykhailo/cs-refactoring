import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (A - ctan(x)) / (B + ctan(y))
Z2 = cos(y + x) / sin(y + x)
Z3 = (Z1 + Z2)^2 + 4 / (A - B)
'''
class Application: #Відокремлення класу
    a = None; b = None; x = None; y = None
    z1 = None; z2 = None; z3 = None
    
    def inputX(self): #Відокремлення методів
        try:
            self.a = float(input("Введіть значення A: "))
            self.b = float(input("Введіть значення B: "))
            self.x = float(input("Введіть значення x: "))
            self.y = float(input("Введіть значення y: "))
        except ValueError:
            print("Некоректні дані. Введіть числові значення для A, B, x та y.")
            sys.exit(1)

    def validation(self): #Заміна алгоритму
        if self.b + math.atan(self.y) == 0 or math.sin(self.y + self.x) == 0 or (self.a - self.b) == 0: #Об'єднання умовних операторів 
            print("Некоректні дані. Знаменник не може дорівнювати нулю.") #Об'єднання фрагментів, що дублюються, в умовних операторах
            sys.exit(1)

    def calc(self):
        self.z1 = (self.a - math.atan(self.x)) / (self.b + math.atan(self.y))
        self.z2 = math.cos(self.y + self.x) / math.sin(self.y + self.x)
        self.z3 = (self.z1 + self.z2) ** 2 + 4 / (self.a - self.b)

    def printX(self):
        print(f"Z1 = {self.z1}")
        print(f"Z2 = {self.z2}")
        print(f"Z3 = {self.z3}")
        sys.exit(0)

def main():
    app = Application()
    app.inputX()
    app.validation()
    app.calc()
    app.printX()
    
if __name__ == "__main__":
    main()
