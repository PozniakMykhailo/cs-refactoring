import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (A - ctan(x)) / (B + ctan(y))
Z2 = cos(y + x)
Z3 = (Z1 + Z2)^2
'''

class Application: #Відокремлення класу
    a = None; b = None; x = None; y = None
    z1 = None; z2 = None; z3 = None
    
    def InputVariables(self): #Відокремлення методу
        try:
            self.a = float(input("Введіть значення A: "))
            self.b = float(input("Введіть значення B: "))
            self.x = float(input("Введіть значення x: "))
            self.y = float(input("Введіть значення y: "))
        except ValueError:
            print("Некоректні дані. Введіть числові значення для A, B, x та y.")
            sys.exit(1)

    def Validation(self): #Заміна параметрів об'єктом???
        if self.b + math.atan(self.y) == 0:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
        
    def Calculation(self):
        self.z1 = (self.a - math.atan(self.x)) / (self.b + math.atan(self.y))
        self.z2 = math.cos(self.y + self.x)
        self.z3 = (self.z1/self.z2) ** 2

    def PrintResults(self): #Перейменування методу
        print(f"Z1 =", self.z1)
        print(f"Z2 =", self.z2)
        print(f"Z3 =", self.z3)
        sys.exit(0)

def main():
    app = Application()
    app.__InputVariables()
    app.Validation()
    app.Calculation()
    app.PrintResults()

if __name__ == "__main__":
    main()
