import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (A - ctan(x)) / (B + ctan(y))
Z2 = cos(y + x)
Z3 = (Z1 + Z2)^2
'''

def validation(b, y):
    if b + math.atan(y) == 0:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
    
def calculation(a, b, x, y):
    z1 = (a - math.atan(x)) / (b + math.atan(y))
    z2 = math.cos(y + x)
    z3 = (z1/z2) ** 2
    results(z1,z2,z3)
    sys.exit(0)

def results(z1,z2,z3):
    print(f"Z1 =", z1)
    print(f"Z2 =", z2)
    print(f"Z3 =", z3)

def main():
    try:
        a = float(input("Введіть значення A: "))
        b = float(input("Введіть значення B: "))
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
    except ValueError:
        print("Некоректні дані. Введіть числові значення для A, B, x та y.")
        sys.exit(1)
    
    validation(b, y)
    calculation(a, b, x, y)

if __name__ == "__main__":
    main()
