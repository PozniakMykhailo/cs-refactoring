import sys
from math import sqrt

"""
Обчислити довжину катета прямокутного трикутника, якщо задано довжини катета та гіпотенузи.
"""

class Application: #Відокремлення класу
    gipotenuza = None; katet = None

    def InputVariable(self): #Відокремлення методу
        try:
            self.gipotenuza = float(input('Введіть довжину гіпотенузи:'))
            self.katet = float(input('Введіть довжину катета:'))
        except ValueError:
            print('Введене значення не є числом.')
            sys.exit(1)

    def Validation(self):
        error_message = None #Додавання параметра

        if self.gipotenuza <= 0:
            error_message = "Довжина гіпотенузи не може бути менше або дорівнювати 0."
        elif self.katet <= 0:
            error_message = "Довжина катета не може бути менше або дорівнювати 0."
        elif self.gipotenuza < self.katet:
            error_message = "Довжина гіпотенузи має бути більшою за довжину катета."
    
        if error_message: #Об'єднання фрагментів, що дублюються, в умовних операторах
            print(error_message)
            sys.exit(1)

    def PrintSecondKatet(self): #Перейменування методу
        print(f"Довжина другого катета дорівнює {sqrt(self.gipotenuza ** 2 - self.katet ** 2)}.")
        sys.exit(0)

def main():
    app = Application()
    app.InputVariable()
    app.Validation()
    app.PrintSecondKatet()

if __name__ == '__main__':
    main()
