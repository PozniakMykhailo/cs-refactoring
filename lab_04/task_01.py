import sys
from math import sqrt

"""
Обчислити довжину катета прямокутного трикутника, якщо задано довжини катета та гіпотенузи.
"""

def validation(v1, v2):
    if  v1 <= 0:
        print(f"Довжина гіпотенузи ({v1}) не може бути менше або дорівнювати 0.")
        sys.exit(1)
    elif v2 <= 0:
        print(f"Довжина катета ({v2}) не може бути менше або дорівнювати 0.")
        sys.exit(1)
    elif v1 < v2:
        print(f"Довжина гіпотенузи ({v1}) має бути більшою за довжину катета {v2}.")
        sys.exit(1)

def printf(v1, v2):
    print(f"Довжина другого катета дорівнює {sqrt(v1 ** 2 - v2 ** 2)}.")
    sys.exit(0)

def inputX():
    try:
        v1 = float(input('Введіть довжину гіпотенузи:'))
        v2 = float(input('Введіть довжину катета:'))
        return v1, v2
    except ValueError:
        print('Введене значення не є числом.')
        sys.exit(1)

def main():
    v1, v2 = inputX()
    validation(v1, v2)
    printf(v1, v2)

if __name__ == '__main__':
    main()
